const homeBase = {
  name: "Australia",
  lat: -24.25,
  lon: 133.416667,
  //   opp: 123.659024,
  //   opp: 57.741, //opposite side of Earth where transfer direction will change
}

const colors = {
  mapLines: "silver",
  mapFill: "#000000",
  workUnitOut: "green",
  workUnitIn: "pink",
  appOut: "#000000",
}

//SETI Data Sources
const dataSources = [
  {
    name: "Australia",
    lat: -24.25,
    lon: 133.416667,
  },
]

var outlocstime = 30000
var inlocstime = 30000
